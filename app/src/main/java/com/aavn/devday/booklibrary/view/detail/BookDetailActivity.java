package com.aavn.devday.booklibrary.view.detail;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.aavn.devday.booklibrary.R;
import com.aavn.devday.booklibrary.viewmodel.BookDetailViewModel;

public class BookDetailActivity extends AppCompatActivity {

    private BookDetailViewModel bookDetailViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_detail);

        bookDetailViewModel.fetchBookDetail();
    }
}
