package com.aavn.devday.booklibrary.view.main;

import com.aavn.devday.booklibrary.data.model.Book;
import com.aavn.devday.booklibrary.data.view.BookView;

public interface BookSelectedListener {
    void onSelect(BookView book);
}
