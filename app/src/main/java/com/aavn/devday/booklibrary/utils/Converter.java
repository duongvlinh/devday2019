package com.aavn.devday.booklibrary.utils;

import com.aavn.devday.booklibrary.data.model.Book;
import com.aavn.devday.booklibrary.data.model.BookDetail;
import com.aavn.devday.booklibrary.data.view.BookView;

import java.util.ArrayList;
import java.util.List;

public final class Converter {
    public static List<BookView> toBookViews (Book book) {
        List<BookView> result = new ArrayList<>();
        if (book == null) {
            return result;
        }

       if (book.getDetails() != null && !book.getDetails().isEmpty()) {
            for (BookDetail detail : book.getDetails()) {
                BookView bookView = new BookView();
                bookView.setId(book.getId());
                bookView.setTitle(book.getTitle());
                bookView.setAuthor(book.getAuthor());
                bookView.setCoverUrl(detail.getCoverUrl());
                bookView.setDescription(detail.getDescription());
                bookView.setSource(detail.getSource());

                result.add(bookView);
            }
        } else {
            BookView bookView = new BookView();
            bookView.setId(book.getId());
            bookView.setTitle(book.getTitle());
            bookView.setAuthor(book.getAuthor());

            result.add(bookView);
        }

        return result;
    }

    public static List<BookView> toBookViews (List<Book> books) {
        List<BookView> result = new ArrayList<>();

        for (Book book : books) {
            result.addAll(toBookViews(book));
        }
        return result;
    }
}
