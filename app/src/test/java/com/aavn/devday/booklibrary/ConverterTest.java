package com.aavn.devday.booklibrary;

import android.app.Application;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.Observer;

import com.aavn.devday.booklibrary.common.JsonHelper;
import com.aavn.devday.booklibrary.common.RxImmediateSchedulerRule;
import com.aavn.devday.booklibrary.data.model.Book;
import com.aavn.devday.booklibrary.data.model.BookDetail;
import com.aavn.devday.booklibrary.data.model.ResponseData;
import com.aavn.devday.booklibrary.data.repository.BookRepository;
import com.aavn.devday.booklibrary.data.view.BookView;
import com.aavn.devday.booklibrary.utils.Converter;
import com.aavn.devday.booklibrary.viewmodel.BookListViewModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class ConverterTest {

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    public List<Book> initialBooks () {
        List<Book> result = new ArrayList<>();

        Book book1 = new Book("Book Test1", "Group6");
        List<BookDetail> details = new ArrayList<>();
        BookDetail detail1 = new BookDetail("Book detail 1");
        BookDetail detail2 = new BookDetail("Book detail 2");

        details.add(detail1);
        details.add(detail2);

        book1.setDetails(details);
        result.add(book1);

        return result;

    }

    @Test
    public void test_convert_bookview_is_success() {
        List<Book> books = initialBooks();
        List<BookView> bookViews = Converter.toBookViews(books);

        assertEquals(2, bookViews.size());
    }

}
